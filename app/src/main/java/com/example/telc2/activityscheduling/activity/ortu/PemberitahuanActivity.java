package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.RecyclerTouchListener;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.NotifModel;
import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.adapter.PemberitahuanAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PemberitahuanActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<AktivitasModel> aktivitasList, aktivitasModelList = new ArrayList<>();
    private List<NotifModel> notifModelList;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberitahuan);

        setTitle("Pemberitahuan");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_pemberitahuan);
        loadNotif();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                NotifModel notifModel = notifModelList.get(position);

                Toast.makeText(getApplicationContext(),
                        "Position :"+position , Toast.LENGTH_SHORT)
                        .show();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)findViewById(R.id.swp_refresh_aktivitas_pemberitahuan);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        /*event ketika widget dijalankan*/
        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                notifModelList.clear();
                loadNotif();
                onItemLoad();
            }

            void onItemLoad() {
                dorefresh.setRefreshing(false);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadNotif() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<NotifResponseModel> getNotif = api.getNotif();
        getNotif.enqueue(new Callback<NotifResponseModel>() {
            @Override
            public void onResponse(Call<NotifResponseModel> call, Response<NotifResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                notifModelList = response.body().getResult();
                PemberitahuanAdapter adapter = new PemberitahuanAdapter(notifModelList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<NotifResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }
}
