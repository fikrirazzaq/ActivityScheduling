package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.PencapaianActivity;
import com.example.telc2.activityscheduling.activity.SettingsActivity;
import com.example.telc2.activityscheduling.activity.anak.HomeAnakEuyActivity;
import com.example.telc2.activityscheduling.adapter.AktivitasAnakAdapter;
import com.example.telc2.activityscheduling.adapter.TahapUbahAdapter;
import com.example.telc2.activityscheduling.helper.DividerItemDecoration;
import com.example.telc2.activityscheduling.helper.RecyclerTouchListener;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahTahapListActivity extends AppCompatActivity {

    private String idAktivitas, judulAktivitas;
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private TextView txJudul;
    private List<TahapModel> tahapModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_tahap_list);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
        }

        setTitle("Ubah Tahap");

        txJudul = (TextView) findViewById(R.id.tx_judul_aktivitas_ubah);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_tahap_ubah);
        loadTahap();

        txJudul.setText(judulAktivitas);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TahapModel tahapModel = tahapModelList.get(position);
                Intent intent = new Intent(UbahTahapListActivity.this, UbahTahapActivity.class);
                intent.putExtra("deskTahap", tahapModel.getThp_desk());
                intent.putExtra("idTahap", tahapModel.getThp_id());
                intent.putExtra("gambarTahap", tahapModel.getThp_gambar());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)findViewById(R.id.swp_refresh_tahap_ubah);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        /*event ketika widget dijalankan*/
        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
//                populateAktivitas();
                loadTahap();
                onItemLoad();
            }

            void onItemLoad() {
                dorefresh.setRefreshing(false);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadTahap() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<TahapResponseModel> getTahap = api.getTahapById(idAktivitas);
        getTahap.enqueue(new Callback<TahapResponseModel>() {
            @Override
            public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                progressDialog.hide();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                tahapModelList = response.body().getResult();
                TahapUbahAdapter adapter = new TahapUbahAdapter(tahapModelList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                progressDialog.hide();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }
}
