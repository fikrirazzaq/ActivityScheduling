package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.TimePickerFragment;
import com.example.telc2.activityscheduling.helper.TimePickerFragment2;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.retrofit.UploadImageResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahAktivitasActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtJudulAktivitas, edtDeskTahap;
    private Button btnWaktu, btnSimpanAktivitas, btnTambahTahap, btnSelesai;
    private RadioGroup radioGroupNb;
    private RadioButton radioButtonNb;
    private ImageButton imbtnAktivitas, imbtnTahap;
    private TextView txTahap, txJudulAktivitasPrev, txTerapiPrev, txWaktuPrev;
    private ImageView imgAktivitasPrev;
    private LinearLayout linearTerapi;
    private RelativeLayout relativeRekam;
    private View divAktivitas1, divAktivitas2, divAktivitas3;
    private int nomortahap = 1;
    private ProgressDialog progressDialog;
    private String judulAktivitas, idAktivitas, waktuAktivitas, gambarAktivitas;


    private String sId;
    private String sJudul;
    private String sWaktu;
    private String sCapaian;
    private String sTerapi;
    private String sPathGambar;
    private String sGambar;
    private File file;
    private String[] mediaColumns = { MediaStore.Video.Media._ID };
    Map<String, Integer> map = new HashMap<String, Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_aktivitas);

        setTitle("Ubah Aktivitas");

        //Pair view
        edtJudulAktivitas = (EditText) findViewById(R.id.edt_ubah_judul_aktivitas);
        btnWaktu = (Button) findViewById(R.id.btn_ubah_pilih_waktu);
        btnSimpanAktivitas = (Button) findViewById(R.id.btn_ubah_simpan_aktivitas_alter);
//        radioGroupNb = (RadioGroup) findViewById(R.id.radioGroupNbUbah);
        imbtnAktivitas = (ImageButton) findViewById(R.id.imbtn_ubah_aktivitas);
//        linearTerapi = (LinearLayout) findViewById(R.id.linearUbahTerapi);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            waktuAktivitas = (String) b.get("waktuAktivitas");
            gambarAktivitas = (String) b.get("gambarAktivitas");
        }

        edtJudulAktivitas.setText(judulAktivitas);
        btnWaktu.setText(waktuAktivitas + "    Setiap Hari");
        Log.d("ASDASDASDASDASD", gambarAktivitas);
        Picasso.with(this).load(gambarAktivitas).into(imbtnAktivitas);

        btnSimpanAktivitas.setOnClickListener(this);
        btnWaktu.setOnClickListener(this);
        imbtnAktivitas.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                sPathGambar = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                imbtnAktivitas.setImageBitmap(BitmapFactory.decodeFile(sPathGambar));
                cursor.close();

            } else {
//                Toast.makeText(this, "Gambar belum dipilih.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == btnWaktu) {
            showTimePickerDialog(v);
        }
        else if (v == imbtnAktivitas) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 0);
        }
        else if (v == btnSimpanAktivitas) {
            uploadFile();

            sId = idAktivitas;
            sJudul = edtJudulAktivitas.getText().toString();
            sWaktu = btnWaktu.getText().toString().substring(0,5);
            sCapaian = "0";
            sTerapi = "0";

            sGambar = "http://juvetic.telclab.com/actscdhl/uploads/" + file.getName();

            Log.d("MERDA", sId + sJudul + sWaktu + sGambar);

            ApiRequest apiAktivitas = RetroServer.getClient().create(ApiRequest.class);
            Call<AktivitasResponseModel> updateAktivitas = apiAktivitas.updateAktivitas(sId, sJudul, sGambar, sWaktu);
            updateAktivitas.enqueue(new Callback<AktivitasResponseModel>() {
                @Override
                public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                    Log.d("Retrofit", "response: " + response.body().toString());
                    String kode = response.body().getKode();
                    Log.d("Merda", "merdi" + kode);
                    if (kode.equals("1")) {
                        Toast.makeText(UbahAktivitasActivity.this, "Data berhasil diperbarui!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(UbahAktivitasActivity.this, UbahTahapListActivity.class);
                        intent.putExtra("idAktivitas", idAktivitas);
                        intent.putExtra("judulAktivitas", judulAktivitas);
                        startActivity(intent);
                    } else {
                        Toast.makeText(UbahAktivitasActivity.this, "Data gagal disimpan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                    Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                }
            });

        }
    }

    // Uploading Image/Video
    private void uploadFile() {
        // Map is used to multipart the file using okhttp3.RequestBody
        file = new File(sPathGambar);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        ApiRequest getResponse = RetroServer.getClient().create(ApiRequest.class);
        Call<UploadImageResponse> call = getResponse.uploadFile(fileToUpload, filename);
        call.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                UploadImageResponse serverResponse = response.body();
                if (serverResponse != null) {
                    if (serverResponse.getSuccess()) {

                    } else {

                    }
                } else {
                    assert serverResponse != null;
                    Log.v("Response", serverResponse.toString());
                }
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {

            }
        });
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment2();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }
}
