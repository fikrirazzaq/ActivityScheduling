package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.PencapaianActivity;
import com.example.telc2.activityscheduling.activity.SettingsActivity;
import com.example.telc2.activityscheduling.adapter.AktivitasAdapter;
import com.example.telc2.activityscheduling.helper.RecyclerTouchListener;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.NotifModel;
import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnBuatAktivitas;
    private RecyclerView recyclerView;
    private List<AktivitasModel> aktivitasList;
    private ProgressDialog progressDialog;
    private List<NotifModel> notifModelList, getNotifModelList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setTitle("");

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        btnBuatAktivitas = (Button) findViewById(R.id.btn_buat_aktivitas);
        btnBuatAktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, BuatActAlterActivity.class);
                startActivity(intent);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_aktivitas);

        loadAktivitasOrtu();
        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<NotifResponseModel> getNotif = api.getNotif();
        getNotif.enqueue(new Callback<NotifResponseModel>() {
            @Override
            public void onResponse(Call<NotifResponseModel> call, Response<NotifResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                notifModelList = response.body().getResult();
                getNotifModelList = notifModelList;
            }

            @Override
            public void onFailure(Call<NotifResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AktivitasModel aktivitas = aktivitasList.get(position);
                Intent intent = new Intent(HomeActivity.this, DetilActActivity.class);
                intent.putExtra("judulAktivitas", aktivitas.getAkt_judul());
                intent.putExtra("idAktivitas", aktivitas.getAkt_id());
                intent.putExtra("waktuAktivitas", aktivitas.getAkt_waktu());
                intent.putExtra("gambarAktivitas", aktivitas.getAkt_gambar());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)findViewById(R.id.swp_refresh_aktivitas_home);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        /*event ketika widget dijalankan*/
        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                loadAktivitasOrtu();
                onItemLoad();
            }

            void onItemLoad() {
                dorefresh.setRefreshing(false);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ortu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notif) {
            Intent intent = new Intent(HomeActivity.this, PemberitahuanActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_pencapaian) {
            Intent intent = new Intent(HomeActivity.this, PencapaianActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_settings) {
            Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
            intent.putExtra("statusOrtu", "1");
            intent.putExtra("statusAnak", "0");
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == android.R.id.home) {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadAktivitasOrtu() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<AktivitasResponseModel> getAktivitasOrtu = api.getAktivitasOrtu();
        getAktivitasOrtu.enqueue(new Callback<AktivitasResponseModel>() {
            @Override
            public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                aktivitasList = response.body().getResult();
                AktivitasAdapter adapter = new AktivitasAdapter(aktivitasList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}