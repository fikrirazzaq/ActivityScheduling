package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.ItemClickListener;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.adapter.TahapOrtuAdapter;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilActActivity extends AppCompatActivity implements ItemClickListener, TextToSpeech.OnInitListener {

    private String judulAktivitas, idAktivitas, waktuAktivitas, gambarAktivitas;
    private TextView txJudul;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private List<TahapModel> tahapModelList;
    private ProgressDialog progressDialog;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_act);

        txJudul = (TextView) findViewById(R.id.tx_judul_aktivitas_detil);

        tts = new TextToSpeech(this, this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            waktuAktivitas = (String) b.get("waktuAktivitas");
            gambarAktivitas = (String) b.get("gambarAktivitas");
        }

        setTitle("");
        txJudul.setText(judulAktivitas);

        loadTahapByAktId(idAktivitas);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_aktivitas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_notif) {
            return true;
        }
        else if (id == R.id.action_informasi) {
            Intent intent = new Intent(DetilActActivity.this, InfoActActivity.class);
            intent.putExtra("judulAktivitas", judulAktivitas);
            intent.putExtra("idAktivitas", idAktivitas);
            intent.putExtra("waktuAktivitas", waktuAktivitas);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_ubah) {
            Intent intent = new Intent(DetilActActivity.this, UbahAktivitasActivity.class);
            intent.putExtra("judulAktivitas", judulAktivitas);
            intent.putExtra("idAktivitas", idAktivitas);
            intent.putExtra("waktuAktivitas", waktuAktivitas);
            intent.putExtra("gambarAktivitas", gambarAktivitas);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_hapus) {
            dialogYesNo();
            return true;
        }
        else if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadTahapByAktId(String idAktivitas) {

        recyclerView = (RecyclerView) findViewById(R.id.recycler_tahap);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<TahapResponseModel> getTahapById = api.getTahapById(idAktivitas);
        getTahapById.enqueue(new Callback<TahapResponseModel>() {
            @Override
            public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                tahapModelList = response.body().getResult();
                TahapOrtuAdapter adapter = new TahapOrtuAdapter(tahapModelList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }

    public void dialogYesNo() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        progressDialog.setMessage("Menghapus aktivitas...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        //Aktivitas
                        ApiRequest apiAkt = RetroServer.getClient().create(ApiRequest.class);
                        Call<AktivitasResponseModel> deleteAktivitas = apiAkt.deleteAktivitas(idAktivitas);
                        deleteAktivitas.enqueue(new Callback<AktivitasResponseModel>() {
                            @Override
                            public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                                Log.d("Retrofit", "Response");
                                progressDialog.hide();
                            }

                            @Override
                            public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                                progressDialog.hide();
                                Log.d("Retrofit", "OnFailure");
                            }
                        });

                        //Tahap
                        ApiRequest apiThp = RetroServer.getClient().create(ApiRequest.class);
                        Call<TahapResponseModel> deleteTahap = apiThp.deleteTahap(idAktivitas);
                        deleteTahap.enqueue(new Callback<TahapResponseModel>() {
                            @Override
                            public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                                Log.d("Retrofit", "Response");
                                Toast.makeText(DetilActActivity.this, "Aktivitas berhasil dihapus!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }

                            @Override
                            public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                                progressDialog.hide();
                                Log.d("Retrofit", "OnFailure");
                            }
                        });
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus aktivitas " + judulAktivitas + "?").setPositiveButton("Ya", dialogClickListener)
                .setNegativeButton("Tidak", dialogClickListener).show();
    }

    @Override
    public void onClick(View view, int position) {
        final TahapModel tahapModel = tahapModelList.get(position);
        String text = tahapModel.getThp_desk();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("id","ID"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }
}