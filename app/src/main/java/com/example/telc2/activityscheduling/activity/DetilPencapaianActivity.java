package com.example.telc2.activityscheduling.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.adapter.DetilPencapaianAdapter;
import com.example.telc2.activityscheduling.adapter.PencapaianAdapter;
import com.example.telc2.activityscheduling.model.PencapaianModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class DetilPencapaianActivity extends AppCompatActivity {

    private String idAktivitas, judulAktivitas, capAktivitas;
    private TextView txJudulPencapaian;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_pencapaian);

        txJudulPencapaian = (TextView) findViewById(R.id.tx_judul_aktivitas_pencapaian);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            capAktivitas = (String) b.get("capAktivitas");
        }

        setTitle("Pencapaian");
        txJudulPencapaian.setText(judulAktivitas);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_capaian);
        ArrayList<PencapaianModel> pencapaianModels = new ArrayList<>();

        Log.d("INASDJASD", capAktivitas);
        for (int i = 1; i <= Integer.parseInt(capAktivitas); i++) {
            pencapaianModels.add(new PencapaianModel(R.mipmap.ic_face, "Capaian " + i));
        }

        DetilPencapaianAdapter adapter = new DetilPencapaianAdapter(pencapaianModels, getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        adapter.notifyDataSetChanged();

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}