package com.example.telc2.activityscheduling.model;

import android.media.Image;

/**
 * Created by juvetic on 6/22/17.
 */

public class TahapModel {

    private String thp_id;
    private String thp_nomor;
    private String thp_gambar;
    private String thp_desk;
    private String akt_id;

    private String nomorTahap;
    private String deskripsiTahap;
    private Image gambarTahap;

    public TahapModel() {
    }

    public TahapModel(String thp_id, String thp_nomor, String thp_gambar, String thp_desk, String akt_id) {
        this.thp_id = thp_id;
        this.thp_nomor = thp_nomor;
        this.thp_gambar = thp_gambar;
        this.thp_desk = thp_desk;
        this.akt_id = akt_id;
    }

    public String getThp_id() {
        return thp_id;
    }

    public void setThp_id(String thp_id) {
        this.thp_id = thp_id;
    }

    public String getThp_nomor() {
        return thp_nomor;
    }

    public void setThp_nomor(String thp_nomor) {
        this.thp_nomor = thp_nomor;
    }

    public String getThp_gambar() {
        return thp_gambar;
    }

    public void setThp_gambar(String thp_gambar) {
        this.thp_gambar = thp_gambar;
    }

    public String getThp_desk() {
        return thp_desk;
    }

    public void setThp_desk(String thp_desk) {
        this.thp_desk = thp_desk;
    }

    public String getAkt_id() {
        return akt_id;
    }

    public void setAkt_id(String akt_id) {
        this.akt_id = akt_id;
    }
}
