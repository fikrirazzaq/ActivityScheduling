package com.example.telc2.activityscheduling.model.response;

import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.TahapModel;

import java.util.List;

/**
 * Created by TELC2 on 7/9/2017.
 */

public class AktivitasResponseModel {

    String kode, pesan;
    List<AktivitasModel> result;

    public List<AktivitasModel> getResult() {
        return result;
    }

    public void setResult(List<AktivitasModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}