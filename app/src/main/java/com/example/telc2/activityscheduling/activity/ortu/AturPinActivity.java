package com.example.telc2.activityscheduling.activity.ortu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.telc2.activityscheduling.R;

public class AturPinActivity extends AppCompatActivity {

    Button btnSelanjutnya;
    EditText edtPin, edtUlangPin, edtPertanyaan, edtJawaban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atur_pin);
        setTitle("Atur PIN Keamanan");

        btnSelanjutnya = (Button) findViewById(R.id.btn_selanjutnya);
        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AturPinActivity.this, AturPinBerhasilActivity.class);
                startActivity(intent);

            }
        });
    }
}