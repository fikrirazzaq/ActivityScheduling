package com.example.telc2.activityscheduling.activity.ortu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.anak.HomeAnakEuyActivity;

public class AturPinBerhasilActivity extends AppCompatActivity {

    private Button btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atur_pin_berhasil);
        setTitle("");

        btnHome = (Button) findViewById(R.id.btn_home);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AturPinBerhasilActivity.this, HomeAnakEuyActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
