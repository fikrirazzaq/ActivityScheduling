package com.example.telc2.activityscheduling.activity.ortu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.SettingsActivity;

public class LupaPinActivity extends AppCompatActivity {

    private TextView txPinKet, txPin;
    private View vwDivider;
    private Button btnSelesai, btnLihatPin;
    private EditText edtJawaban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_pin);
        setTitle("Lupa Pin");

        txPinKet = (TextView) findViewById(R.id.tx_pin_aktif_ket);
        txPin = (TextView) findViewById(R.id.tx_pin_aktif);
        vwDivider = findViewById(R.id.vw_divider_lihat_pin);
        btnSelesai = (Button) findViewById(R.id.btn_selesai);
        btnLihatPin = (Button) findViewById(R.id.btn_lihat_pin);
        edtJawaban = (EditText) findViewById(R.id.edt_jawab_lupa);

        btnLihatPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtJawaban.getText().toString().equals("Agus")) {
                    txPinKet.setVisibility(View.VISIBLE);
                    txPin.setVisibility(View.VISIBLE);
                    vwDivider.setVisibility(View.VISIBLE);
                    btnSelesai.setVisibility(View.VISIBLE);
                }
                else {
                    edtJawaban.setError("Jawaban Salah");
                }
            }
        });

        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LupaPinActivity.this, SettingsActivity.class);
                intent.putExtra("statusOrtu", "1");
                intent.putExtra("statusAnak", "0");
                sendBroadcast(intent);
                startActivity(intent);
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}