package com.example.telc2.activityscheduling.model.response;

import com.example.telc2.activityscheduling.model.NotifModel;

import java.util.List;

/**
 * Created by TELC2 on 7/10/2017.
 */

public class NotifResponseModel {
    String kode, pesan;
    List<NotifModel> result;

    public List<NotifModel> getResult() {
        return result;
    }

    public void setResult(List<NotifModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
