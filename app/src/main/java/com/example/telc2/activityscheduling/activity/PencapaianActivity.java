package com.example.telc2.activityscheduling.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.adapter.AktivitasAdapter;
import com.example.telc2.activityscheduling.helper.RecyclerTouchListener;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PencapaianActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private List<AktivitasModel> aktivitasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencapaian);
        setTitle("Pencapaian");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_aktivitas_anaking);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(true);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<AktivitasResponseModel> getAktivitasOrtu = api.getAktivitasOrtu();
        getAktivitasOrtu.enqueue(new Callback<AktivitasResponseModel>() {
            @Override
            public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                aktivitasList = response.body().getResult();
                AktivitasAdapter adapter = new AktivitasAdapter(aktivitasList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AktivitasModel aktivitas = aktivitasList.get(position);
                Intent intent = new Intent(PencapaianActivity.this, DetilPencapaianActivity.class);
                intent.putExtra("judulAktivitas", aktivitas.getAkt_judul());
                intent.putExtra("idAktivitas", aktivitas.getAkt_id());
                intent.putExtra("capAktivitas", aktivitas.getAkt_capaian());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}