package com.example.telc2.activityscheduling.activity.ortu;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.retrofit.UploadImageResponse;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahTahapActivity extends AppCompatActivity {

    private String idTahap, deskTahap, gambarTahap;
    private EditText edtTahap;
    private ImageButton imgBtnTahap;
    private Button btnTahap;
    private String sId;
    private String sDeskp;
    private String sWaktu;
    private String sCapaian;
    private String sTerapi;
    private String sPathGambar;
    private String sGambar;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_tahap);
        setTitle("");

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            idTahap = (String) b.get("idTahap");
            deskTahap = (String) b.get("deskTahap");
            gambarTahap = (String) b.get("gambarTahap");
        }

        edtTahap = (EditText) findViewById(R.id.edt_deskr_tahap_ubah);
        imgBtnTahap = (ImageButton) findViewById(R.id.imbtn_tahap_ubah);
        btnTahap = (Button) findViewById(R.id.btn_tahap_ubah);

        edtTahap.setText(deskTahap);
        Picasso.with(this).load(gambarTahap).into(imgBtnTahap);

        btnTahap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();

                sId = idTahap;
                sDeskp = edtTahap.getText().toString();

                sGambar = "http://juvetic.telclab.com/actscdhl/uploads/" + file.getName();

                ApiRequest apiTahap = RetroServer.getClient().create(ApiRequest.class);
                Call<TahapResponseModel> updateTahap = apiTahap.updateTahap(sId, sGambar, sDeskp);
                updateTahap.enqueue(new Callback<TahapResponseModel>() {
                    @Override
                    public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        Log.d("Merda", "merdi" + kode);
                        if (kode.equals("1")) {
                            Toast.makeText(UbahTahapActivity.this, "Data berhasil diperbarui!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UbahTahapActivity.this, "Data gagal disimpan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });
            }
        });

        imgBtnTahap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                sPathGambar = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                imgBtnTahap.setImageBitmap(BitmapFactory.decodeFile(sPathGambar));
                cursor.close();

            } else {
//                Toast.makeText(this, "Gambar belum dipilih.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }
    }

    // Uploading Image/Video
    private void uploadFile() {
        // Map is used to multipart the file using okhttp3.RequestBody
        file = new File(sPathGambar);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        ApiRequest getResponse = RetroServer.getClient().create(ApiRequest.class);
        Call<UploadImageResponse> call = getResponse.uploadFile(fileToUpload, filename);
        call.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                UploadImageResponse serverResponse = response.body();
                if (serverResponse != null) {
                    if (serverResponse.getSuccess()) {

                    } else {

                    }
                } else {
                    assert serverResponse != null;
                    Log.v("Response", serverResponse.toString());
                }
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {

            }
        });
    }
}
