package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TEL-C on 8/17/17.
 */

public class TahapUbahAdapter extends RecyclerView.Adapter<TahapUbahAdapter.HolderData> {

    private List<TahapModel> tahapModelList;
    Context mContext;

    public TahapUbahAdapter(List<TahapModel> tahapModelList, Context mContext) {
        this.tahapModelList = tahapModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tahap_ubah, parent, false);
        TahapUbahAdapter.HolderData holder = new TahapUbahAdapter.HolderData(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        holder.txTahapUbah.setText(tahapModelList.get(position).getThp_desk());
        Picasso.with(mContext).load(tahapModelList.get(position).getThp_gambar()).into(holder.imgTahapUbah);
    }

    @Override
    public int getItemCount() {
        return tahapModelList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {

        private ImageView imgTahapUbah;
        private TextView txTahapUbah;

        public HolderData(View itemView) {
            super(itemView);

            imgTahapUbah = (ImageView) itemView.findViewById(R.id.img_tahap_ubah_list);
            txTahapUbah = (TextView) itemView.findViewById(R.id.tx_judul_tahap_ubah_list);


        }
    }
}
