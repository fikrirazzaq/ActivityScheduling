package com.example.telc2.activityscheduling.activity.ortu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.activity.SettingsActivity;

public class MasukOrtuActivity extends AppCompatActivity {

    private Button btnMasuk, btnLupaPin;
    private EditText edtPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masuk_ortu);
        setTitle("Input PIN");

        btnLupaPin = (Button) findViewById(R.id.btn_lupa_pin);
        btnMasuk = (Button) findViewById(R.id.btn_masuk);
        edtPin = (EditText) findViewById(R.id.edt_masuk_pin);

        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("7878")) {
                    Intent intent = new Intent(MasukOrtuActivity.this, SettingsActivity.class);
                    intent.putExtra("statusOrtu", "1");
                    intent.putExtra("statusAnak", "0");
                    sendBroadcast(intent);
                    startActivity(intent);
                    finish();
                }
                else {
                    edtPin.setError("Pin Salah");
                }
            }
        });

        btnLupaPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MasukOrtuActivity.this, LupaPinActivity.class);
                startActivity(intent);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
