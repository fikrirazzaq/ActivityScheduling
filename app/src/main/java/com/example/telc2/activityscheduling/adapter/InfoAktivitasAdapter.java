package com.example.telc2.activityscheduling.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.NotifModel;
import com.example.telc2.activityscheduling.model.TahapModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by TELC2 on 7/17/2017.
 */

public class InfoAktivitasAdapter extends RecyclerView.Adapter<InfoAktivitasAdapter.HolderData> {

    List<NotifModel> notifModelList;
    Context mContext;

    public InfoAktivitasAdapter(List<NotifModel> notifModelList, Context mContext) {
        this.notifModelList = notifModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_info_act, parent, false);
        InfoAktivitasAdapter.HolderData holder = new InfoAktivitasAdapter.HolderData(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        holder.txInfoAct.setText(""+(position+1));
        holder.txWaktuTgl.setText(notifModelList.get(position).getWaktu().substring(0, 8));
        holder.txWaktuJam.setText(notifModelList.get(position).getWaktu().substring(9).substring(0, 5));
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
//        Date date = new Date();
//        holder.txWaktuMulai.setText(dateFormat.format(date));
    }

    @Override
    public int getItemCount() {
        return notifModelList.size();
    }

    class HolderData extends RecyclerView.ViewHolder {

        TextView txWaktuTgl, txWaktuJam, txInfoAct;

        public HolderData(View itemView) {
            super(itemView);

            txInfoAct = (TextView) itemView.findViewById(R.id.tx_urut_info_act);
            txWaktuTgl = (TextView) itemView.findViewById(R.id.tx_tgl_aktivitas_info);
            txWaktuJam = (TextView) itemView.findViewById(R.id.tx_jam_aktivitas_info);
//            txWaktuMulai = (TextView) itemView.findViewById(R.id.tx_lama_aktivitas_info);
        }
    }
}
