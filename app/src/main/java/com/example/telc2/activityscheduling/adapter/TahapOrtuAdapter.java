package com.example.telc2.activityscheduling.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.ItemClickListener;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by TELC2 on 7/9/2017.
 */

public class TahapOrtuAdapter extends RecyclerView.Adapter<TahapOrtuAdapter.HolderData> {

    private List<TahapModel> tahapModelList;
    private Context mContext;
    private ItemClickListener clickListener;
    TextToSpeech tts;

    public TahapOrtuAdapter(List<TahapModel> tahapModelList, Context mContext) {
        this.tahapModelList = tahapModelList;
        this.mContext = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tahap, parent, false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(final HolderData holder, int position) {
        TahapModel tahapModel = tahapModelList.get(position);
        holder.txNomorTahap.setText("Tahap " + tahapModel.getThp_nomor());
        holder.txDeskripsiTahap.setText(tahapModel.getThp_desk());
        Picasso.with(mContext).load(tahapModel.getThp_gambar()).into(holder.imgTahap);

        holder.btnSuaraTahap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tts = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status == TextToSpeech.SUCCESS) {
                            tts.setLanguage(new Locale("id","ID"));
                            playNextChunk(holder.txDeskripsiTahap.getText().toString());
                        }
                    }
                });
            }
        });
    }

    private void playNextChunk(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(text);
        } else {
            ttsUnder20(text);
        }
    }

    @SuppressWarnings("deprecation")
    private void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void ttsGreater21(String text) {
        String utteranceId = this.hashCode() + "";
        Bundle params = new Bundle();
        params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "");
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, params, utteranceId);
    }

    public void setClickListener(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return tahapModelList.size();
    }

    class HolderData extends RecyclerView.ViewHolder {

        TextView txNomorTahap, txDeskripsiTahap;
        ImageView imgTahap;
        ImageButton btnSuaraTahap;
        TextToSpeech tts;

        public HolderData(View itemView) {
            super(itemView);
            txNomorTahap = (TextView) itemView.findViewById(R.id.tx_nomor_tahap_ortu);
            txDeskripsiTahap = (TextView) itemView.findViewById(R.id.tx_desk_tahap_ortu);
            imgTahap = (ImageView) itemView.findViewById(R.id.img_detil_tahap_ortu);
            btnSuaraTahap = (ImageButton) itemView.findViewById(R.id.btn_suara_tahap_ortu);

        }

    }
}