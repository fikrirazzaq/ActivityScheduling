package com.example.telc2.activityscheduling.activity.anak;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.model.TahapModel;
import com.example.telc2.activityscheduling.model.response.AktivitasResponseModel;
import com.example.telc2.activityscheduling.model.response.TahapResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.adapter.TahapOrtuAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetilTahap extends AppCompatActivity {

    private String judulAktivitas, idAktivitas, capaianAktivitas;
    private List<TahapModel> tahapModelList;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_tahap);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            capaianAktivitas = (String) b.get("capaianAktivitas");
        }

        setTitle(judulAktivitas);
        loadTahapByAktId(idAktivitas);

        Button btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int capai = Integer.parseInt(capaianAktivitas);
                capai++;
                String sCapai = Integer.toString(capai);

                ApiRequest apiTahap = RetroServer.getClient().create(ApiRequest.class);
                Call<AktivitasResponseModel> updateCapaian = apiTahap.updateCapaian(sCapai, "1", idAktivitas);
                updateCapaian.enqueue(new Callback<AktivitasResponseModel>() {
                    @Override
                    public void onResponse(Call<AktivitasResponseModel> call, Response<AktivitasResponseModel> response) {
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {
                            Toast.makeText(DetilTahap.this, "Aktivitas selesai!", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(DetilTahap.this, "Aktivitas gagal diselesaikan!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AktivitasResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });

                //Insert Notif
                ApiRequest apiNotif = RetroServer.getClient().create(ApiRequest.class);
                Call<NotifResponseModel> insertNotif = apiNotif.insertNotif(now(), "Anak Anda menyelesaikan " + judulAktivitas, idAktivitas);
                insertNotif.enqueue(new Callback<NotifResponseModel>() {
                    @Override
                    public void onResponse(Call<NotifResponseModel> call, Response<NotifResponseModel> response) {
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        if (kode.equals("1")) {
                            finish();
                        } else {
                            Toast.makeText(DetilTahap.this, "Terjadi kesalahan jaringan!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<NotifResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });

                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    Get Tahap by Aktivitas Id
    public void loadTahapByAktId(String idAktivitas) {

        recyclerView = (RecyclerView) findViewById(R.id.recycler_tahap_anak);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<TahapResponseModel> getTahapById = api.getTahapById(idAktivitas);
        getTahapById.enqueue(new Callback<TahapResponseModel>() {
            @Override
            public void onResponse(Call<TahapResponseModel> call, Response<TahapResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                tahapModelList = response.body().getResult();
                TahapOrtuAdapter adapter = new TahapOrtuAdapter(tahapModelList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TahapResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }

//    Waktu sekarang
    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }
}