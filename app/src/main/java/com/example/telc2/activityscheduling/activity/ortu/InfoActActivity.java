package com.example.telc2.activityscheduling.activity.ortu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.telc2.activityscheduling.R;
import com.example.telc2.activityscheduling.helper.DividerItemDecoration;
import com.example.telc2.activityscheduling.model.AktivitasModel;
import com.example.telc2.activityscheduling.model.NotifModel;
import com.example.telc2.activityscheduling.model.response.NotifResponseModel;
import com.example.telc2.activityscheduling.retrofit.ApiRequest;
import com.example.telc2.activityscheduling.retrofit.RetroServer;
import com.example.telc2.activityscheduling.adapter.InfoAktivitasAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoActActivity extends AppCompatActivity {

    private String judulAktivitas, idAktivitas, waktuAktivitas;
    private TextView txJudulAktivitas, txWaktuAktivitas;
    private List<AktivitasModel> aktivitasList = new ArrayList<>();
    private RecyclerView recyclerView;
    private List<NotifModel> notifModelList;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_act);
        setTitle("Informasi Aktivitas");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_info_aktivitas);

        txJudulAktivitas = (TextView) findViewById(R.id.tx_judul_aktivitas_info);
        txWaktuAktivitas = (TextView) findViewById(R.id.tx_waktu_aktivitas_info);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            judulAktivitas = (String) b.get("judulAktivitas");
            idAktivitas = (String) b.get("idAktivitas");
            waktuAktivitas = (String) b.get("waktuAktivitas");
        }

        loadListPengerjaan(idAktivitas);

        txJudulAktivitas.setText(judulAktivitas);
        txWaktuAktivitas.setText(waktuAktivitas);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)findViewById(R.id.swp_refresh_aktivitas_infoakt);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        /*event ketika widget dijalankan*/
        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                loadListPengerjaan(idAktivitas);
                onItemLoad();
            }

            void onItemLoad() {
                dorefresh.setRefreshing(false);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadListPengerjaan(String idAktivitas) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mengambil data...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
        Call<NotifResponseModel> getNotif = api.getNotifById(idAktivitas);
        getNotif.enqueue(new Callback<NotifResponseModel>() {
            @Override
            public void onResponse(Call<NotifResponseModel> call, Response<NotifResponseModel> response) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Response: " + response.body().getKode());
                notifModelList = response.body().getResult();
                InfoAktivitasAdapter adapter = new InfoAktivitasAdapter(notifModelList, getApplicationContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<NotifResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Retrofit", "Failure: Response Gagal");
            }
        });
    }
}
